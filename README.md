__A Scriptographer Script__

The aim is to automate the process of generating the logotype in a similar style to that lovingly crafted by Mr Mac.

http://endofyearshow.gdnm.org/files/2012/03/1.jpg 

__Getting Started__

First up you're going to want to head over to the project's homepage and download Scriptographer, they've got comprehensive installation and introduction documentation there as well.

http://scriptographer.org/

__Usage__

Pretty simplistic

- Add a type object into the page, make sure it's selected.
- Play the logomark.js script


